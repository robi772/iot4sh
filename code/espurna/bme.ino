/*

BME280 Module

Copyright (C) 2016-2017 by Robert Juhasz

*/

#if ENABLE_BME

#include <Adafruit_Sensor.h>
#include <BME280I2C.h>

/* ==== Global Variables ==== */
BME280I2C bme;                   // Default : forced mode, standby time = 1000 ms
                              // Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,
bool metric = true;
/* ==== END Global Variables ==== */

#define BME_UPDATE_INTERVAL     10000
#define BME_TEMPERATURE_TOPIC   "temperature"
#define BME_HUMIDITY_TOPIC      "humidity"
#define BME_PRESSURE_TOPIC      "pressure"

double _bmeTemperature = 0;
double _bmePressure = 0;
unsigned int _bmeHumidity = 0;

// -----------------------------------------------------------------------------
// Values
// -----------------------------------------------------------------------------

double getBMETemperature() {
    return _bmeTemperature;
}

unsigned int getBMEHumidity() {
    return _bmeHumidity;
}

unsigned int getBMEPressure() {
    return _bmePressure;
}

void bmeSetup() {

    if (bme.begin(12,14)) {
        DEBUG_MSG("[BME] Started successfull\n");
    }
    else {
        DEBUG_MSG("[BME] Something wrong, no device\n");
    }
    apiRegister("/api/temperature", "temperature", [](char * buffer, size_t len) {
        dtostrf(_bmeTemperature, len-1, 1, buffer);
    });
    apiRegister("/api/humidity", "humidity", [](char * buffer, size_t len) {
        snprintf(buffer, len, "%d", _bmeHumidity);
    });
    apiRegister("/api/pressure", "pressure", [](char * buffer, size_t len) {
        snprintf(buffer, len, "%d", _bmePressure);
    });
}

void bmeLoop() {

    // Check if we should read new data
    static unsigned long last_update = 0;
    if ((millis() - last_update > BME_UPDATE_INTERVAL) || (last_update == 0)) {
        last_update = millis();

        unsigned char tmpUnits = getSetting("tmpUnits", TMP_UNITS).toInt();

        // Read sensor data
        double h = bme.hum();
        double t = bme.temp(true);
        double p = bme.pres(1);

        // Check if readings are valid
        if (isnan(h) || isnan(t)) {

            DEBUG_MSG("[BME] Error reading sensor\n");

        } else {

            _bmeTemperature = t;
            _bmeHumidity = h;
            _bmePressure = p;

            char temperature[6];
            char humidity[6];
            char pressure[6];
            dtostrf(t, 4, 1, temperature);
            dtostrf(p, 4, 1, pressure);
            itoa((unsigned int) h, humidity, 10);

            DEBUG_MSG("[BME] Temperature: %s%s\n", temperature, (tmpUnits == TMP_CELSIUS) ? "ºC" : "ºF");
            DEBUG_MSG("[BME] Humidity: %s\n", humidity);
            DEBUG_MSG("[BME] Pressure: %s hPa\n", pressure);

            // Send MQTT messages
            mqttSend(getSetting("bmeTmpTopic", BME_TEMPERATURE_TOPIC).c_str(), temperature);
            mqttSend(getSetting("bmeHumTopic", BME_HUMIDITY_TOPIC).c_str(), humidity);
            mqttSend(getSetting("bmePressTopic", BME_PRESSURE_TOPIC).c_str(), pressure);

            // Send to Domoticz
            #if ENABLE_DOMOTICZ
            {
                domoticzSend("dczBMETmpIdx", 0, temperature);
                int status;
                if (h > 70) {
                    status = HUMIDITY_WET;
                } else if (h > 45) {
                    status = HUMIDITY_COMFORTABLE;
                } else if (h > 30) {
                    status = HUMIDITY_NORMAL;
                } else {
                    status = HUMIDITY_DRY;
                }
                char buffer[2];
                sprintf(buffer, "%d", status);
                domoticzSend("dczBMEHumIdx", humidity, buffer);
                domoticzSend("dczBMEPressIdx",0, pressure);
            }
            #endif

            // Update websocket clients
            char buffer[100];
            sprintf_P(buffer, PSTR("{\"dhtVisible\": 1, \"dhtTmp\": %s, \"dhtHum\": %s, \"tmpUnits\": %d, \"dhtPress\": %s}"), temperature, humidity, tmpUnits,pressure);
            wsSend(buffer);

        }

    }

}

#endif
