/*

DS18B20 MODULE

Copyright (C) 2016-2017 by Xose Pérez <xose dot perez at gmail dot com>

*/

#if ENABLE_DS18B20M

#include <OneWire.h>
#include <DallasTemperature.h>

OneWire oneWire(DS_PIN);
DallasTemperature ds18b20(&oneWire);

bool _dsIsConnected = false;
double _dsTemperature = 0;
double _dsTemperatureM[10];
int activeSensor=0;
char _dsTemperatureStr[10][10];
// arrays to hold device addresses
//deviceAddress dsSensors[10];


// -----------------------------------------------------------------------------
// DS18B20
// -----------------------------------------------------------------------------

double * getDSTemperatureM() {
    return _dsTemperatureM;
}

int getDSTemperatureCount() {
    return activeSensor;
}

const char* getDSTemperatureStr(int idx) {
    if (!_dsIsConnected)
        return "NOT CONNECTED";

    return _dsTemperatureStr[idx];
}



void dsSetupM() {
    ds18b20.begin();
    ds18b20.setWaitForConversion(false);
    activeSensor = ds18b20.getDeviceCount();
    DEBUG_MSG("[DS18B20M] found sensor %d\n",activeSensor);

    apiRegister("/api/temperature", "temperature", [](char * buffer, size_t len) {
        dtostrf(_dsTemperature, len-1, 1, buffer);
      });
}

void dsLoopM() {

    // Check if we should read new data
    static unsigned long last_update = 0;
    static bool requested = false;
    if ((millis() - last_update > DS_UPDATE_INTERVAL) || (last_update == 0)) {
      if (!requested) {
          ds18b20.requestTemperatures();
          requested = true;

          /* Requesting takes time,
           * so data will probably not be available in this round */
          return;
      }

      /* Check if requested data is already available */
      if (!ds18b20.isConversionComplete()) {
          return;
      }

      requested = false;
        last_update = millis();

        unsigned char tmpUnits = getSetting("tmpUnits", TMP_UNITS).toInt();

        // Read sensor data
        int i;
        double x[10];
        for(i=0;i<activeSensor;i++){

          double t = (tmpUnits == TMP_CELSIUS) ? ds18b20.getTempCByIndex(i) : ds18b20.getTempFByIndex(i);

        // Check if readings are valid
        if (isnan(t)) {

            DEBUG_MSG("[DS18B20M] Error reading sensor\n");

        } else {

          _dsTemperature = t;

          if ((tmpUnits == TMP_CELSIUS && _dsTemperature == DEVICE_DISCONNECTED_C) ||
              (tmpUnits == TMP_FAHRENHEIT && _dsTemperature == DEVICE_DISCONNECTED_F))
            _dsIsConnected = false;
          else
            _dsIsConnected = true;

          dtostrf(t, 5, 1, _dsTemperatureStr[i]);
        //  dtostrf(t, 5, 1, _dsTemperatureStr);

          DEBUG_MSG("[DS18B20M] Temperature: %s%s\n",
              getDSTemperatureStr(i),
        (_dsIsConnected ? ((tmpUnits == TMP_CELSIUS) ? "ºC" : "ºF") : ""));

          // Send MQTT messages TODO
          mqttSend(getSetting("dsTmpTopic", DS_TEMPERATURE_TOPIC).c_str(),i, _dsTemperatureStr[i]);

          // Send to Domoticz
          #if ENABLE_DOMOTICZ
          //    domoticzSend("dczTmpIdx", 0, _dsTemperatureStr[i]);

              char dzbuffer[15];
              sprintf(dzbuffer, "dczdsMIdx%d", i);
              domoticzSend(dzbuffer,0, _dsTemperatureStr[i]);
          #endif

          // Update websocket clients
          char buffer[100];
          sprintf_P(buffer, PSTR("{\"dsVisibleM\": 1, \"dsMTmp\": %s, \"tmpUnits\": %d, \"dsMid\": %d}"), getDSTemperatureStr(i), tmpUnits,i);
          wsSend(buffer);

        }
      }
    }

}


#if ENABLE_DOMOTICZ

void dsMDomoticzSend(unsigned int dsMID) {
    char buffer[15];
    sprintf(buffer, "dczdsMIdx%d", dsMID);
    domoticzSend(buffer, getDSTemperatureStr(dsMID) );
}

int dsMFromIdx(unsigned int idx) {
    for (int dsMID=0; dsMID<getDSTemperatureCount(); dsMID++) {
        if (dsMToIdx(dsMID) == idx) {
            return dsMID;
        }
    }
    return -1;
}

int dsMToIdx(unsigned int dsMID) {
    char buffer[15];
    sprintf(buffer, "dczdsMIdx%d", dsMID);
    return getSetting(buffer).toInt();
}

#endif

#endif
