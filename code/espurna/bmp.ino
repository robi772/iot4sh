/*

BMP280 Module

Copyright (C) 2016-2017 by Robert Juhasz

*/

#if ENABLE_BMP

#include <Adafruit_Sensor.h>
#include <BME280I2C.h>

/* ==== Global Variables ==== */
BME280I2C bme;                   // Default : forced mode, standby time = 1000 ms
                              // Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,
bool metric = true;
/* ==== END Global Variables ==== */

#define BME_UPDATE_INTERVAL     10000
#define BME_TEMPERATURE_TOPIC   "temperature"
#define BME_PRESSURE_TOPIC      "pressure"

double _bmeTemperature = 0;
double _bmePressure = 0;


// -----------------------------------------------------------------------------
// Values
// -----------------------------------------------------------------------------

double getBMETemperature() {
    return _bmeTemperature;
}


unsigned int getBMEPressure() {
    return _bmePressure;
}

void bmpSetup() {

    if (bme.begin(12,14)) {
        DEBUG_MSG("[BMp] Started successfull\n");
    }
    else {
        DEBUG_MSG("[BMP] Something wrong, no device\n");
    }
    apiRegister("/api/temperature", "temperature", [](char * buffer, size_t len) {
        dtostrf(_bmeTemperature, len-1, 1, buffer);
    });
    apiRegister("/api/pressure", "pressure", [](char * buffer, size_t len) {
        snprintf(buffer, len, "%d", _bmePressure);
    });
}

void bmpLoop() {

    // Check if we should read new data
    static unsigned long last_update = 0;
    if ((millis() - last_update > BME_UPDATE_INTERVAL) || (last_update == 0)) {
        last_update = millis();

        unsigned char tmpUnits = getSetting("tmpUnits", TMP_UNITS).toInt();

        // Read sensor data

        double t = bme.temp(true);
        double p = bme.pres(1);

        // Check if readings are valid
        if (isnan(t)) {

            DEBUG_MSG("[BMP] Error reading sensor\n");

        } else {

            _bmeTemperature = t;

            _bmePressure = p;

            char temperature[6];
            char pressure[6];
            dtostrf(t, 4, 1, temperature);
            dtostrf(p, 4, 1, pressure);


            DEBUG_MSG("[BMP] Temperature: %s%s\n", temperature, (tmpUnits == TMP_CELSIUS) ? "ºC" : "ºF");
            DEBUG_MSG("[BMP] Pressure: %s hPa\n", pressure);

            // Send MQTT messages
            mqttSend(getSetting("bmeTmpTopic", BME_TEMPERATURE_TOPIC).c_str(), temperature);
            mqttSend(getSetting("bmePressTopic", BME_PRESSURE_TOPIC).c_str(), pressure);

            // Send to Domoticz
            #if ENABLE_DOMOTICZ
            {
                domoticzSend("dczHTmpIdx", 0, temperature);
                domoticzSend("dczPressIdx",0, pressure);
            }
            #endif

            // Update websocket clients
            char buffer[100];
            sprintf_P(buffer, PSTR("{\"bmpVisible\": 1, \"bmpTmp\": %s, \"tmpUnits\": %d, \"bmpPress\": %s}"), temperature, tmpUnits,pressure);
            wsSend(buffer);

        }

    }

}

#endif
